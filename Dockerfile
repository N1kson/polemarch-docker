FROM centos:7

ARG DOWNLOAD_URL
RUN yum update -y && yum install -y nano sshpass
RUN yum install -y ${DOWNLOAD_URL}

ENV SECRET_KEY='DISABLE'
ENV WORKER='ENABLE'
ADD ./ConfGen.py /confgen.py
RUN chmod a+x /confgen.py

RUN yum clean all && rm -rf /var/cache/yum

EXPOSE 8080
CMD [ "/opt/polemarch/bin/python", "/confgen.py"]