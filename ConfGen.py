import configparser
import os
from subprocess import check_call

for dir in ['/projects', '/hooks']:
    if not os.path.exists(dir):
        os.makedirs(dir)

config = configparser.ConfigParser()
log_level = os.getenv('POLEMARCH_LOG_LEVEL', 'WARNING')
config['main'] = {
    'debug': os.getenv('POLEMARCH_DEBUG', 'false'),
    'log_level': log_level,
    'timezone': os.getenv('POLEMARCH_TIMEZONE', 'UTC'),
    'projects_dir': '/projects',
    'hooks_dir': '/hooks'
}

if os.getenv('POLEMARCH_DB_HOST') != None:
    try:
        config['database'] = {
            'engine': 'django.db.backends.mysql',
            'name': os.environ['POLEMARCH_DB_NAME'],
            'user': os.environ['POLEMARCH_DB_USER'],
            'password': os.environ['POLEMARCH_DB_PASSWORD'],
            'host': os.environ['POLEMARCH_DB_HOST'],
            'port': os.getenv('POLEMARCH_DB_PORT', '3306')
        }
    except KeyError:
        raise Exception('Get not enough vars for MYSQL server.')
else:
    config['database'] = {
        'engine': 'django.db.backends.sqlite3',
        'name': '/db.sqlite3'
    }

try:
    config['database.options'] = {'init_command': os.environ['DB_INIT_CMD']}
except:
    pass

cache_loc = os.getenv('CACHE_LOCATION', '/tmp/polemarch_django_cache')
if cache_loc == '/tmp/polemarch_django_cache':
    cache_engine = 'django.core.cache.backends.filebased.FileBasedCache'
else:
    cache_engine = 'django.core.cache.backends.memcached.MemcachedCache'

config['cache'] = config['locks'] = {
    'backend': cache_engine,
    'location': cache_loc
}

rpc_connection = os.getenv('RPC_ENGINE', None)
config['rpc'] = {
    'heartbeat': os.getenv('RPC_HEARTBEAT', '5'),
    'concurrency': os.getenv('RPC_CONCURRENCY', '4')
}
if rpc_connection:
    config['rpc']['connection'] = rpc_connection

config['web'] = {
    'allowed_hosts': '*',
    'static_files_url': '/static/',
    'rest_page_limit': os.getenv('POLEMARCH_WEB_REST_PAGE_LIMIT', '1000')
}

config['uwsgi'] = {
    'processes': os.getenv('POLEMARCH_UWSGI_PROCESSES', '4'),
    'threads': '2',
    'pidfile': '/run/web.pid',
    'daemon': 'false'
}
if os.environ['WORKER'] == 'ENABLE':
    config['uwsgi']['attach-daemon'] = (
        r'/opt/polemarch/bin/celery worker '
        '-A polemarch.wapp:app -B -l {} '
        r'--pidfile=/tmp/worker.pid '
        r'--schedule=/tmp/beat-schedule'
    ).format(log_level)


if os.environ['SECRET_KEY'] != 'DISABLE':
    with open('/etc/polemarch/secret', 'w') as secretfile:
        secretfile.write(os.environ['SECRET_KEY'])

with open('/etc/polemarch/settings.ini', 'w') as configfile:
    config.write(configfile)

check_call(['/opt/polemarch/bin/polemarchctl', 'migrate'])
check_call(['/opt/polemarch/bin/polemarchctl', 'webserver'])
